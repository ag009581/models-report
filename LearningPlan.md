# Front page of the submission
<div align="center">

Module Code: **CS1SE20**

Assignment report Title: **Models Report**

Student Number(s): **29009581**

Date: **12<sup>nd</sup> November 2020**

Actual hrs spent for assignment: **~16**

Assignment Evaluation: **assignment researched, understood and completed**

</div>

# Content of the required work
# Models Report

## Abstraction

This project aims to breakdown the **Computer Science (BSc)** degree into an abstracted form that is simple to understand but still contain the core features and information about the course as a whole, so that we are able to implement Software Engineering skills. We use these skills and concepts of Software Engineering in order to create a learning plan to create a final product of high quality and the importance of having this **Computer Science (BSc)** degree.
 This project also aims to identifying the pros and cons of undertaking this degree. I will breakdown the course as a **Work Breakdown Structure** and as a **Gantt Chart** as it is a form of visualization which is very beneficial to humans as it is quicker for humans to interpret and highlight the necessary relevant data in an organized manner.  

Sources have been referenced below and work used from sites has been highlighted in italic with links to the sites referenced from, this report also contains the writer CV without name and personal information for fairness and privacy reasons.

## Introduction

In this project we are going to decompose the Computer Science (BSc) degree, which is a academic degree which is _“an academic course and qualification studied for and attained at university – a bachelor's degree or master's degree.”_  **[1]**. Degrees are normally a combination of lecture or lab sessions combines with coursework and exams. Durations of bachelor’s degree require 3 years of study while in Scotland a bachelors degree would require 4 years. Degrees are available at numerous levels of difficulty and required years of study such as:

*	First-class honours
*	Second-class honours, upper division
*	Second-class honours, lower division 
*	Third-class honours 
*	Ordinary degree

Each specific degree has sub modules of 6 with each model worth 20 credits and a student would need 120 credits to pass the specific academic year. A module of 20 credits would require the student to spend at least 200 hours on the model in a academic year to be able to understand and work through the assessments and exams, this would in turn mean that the student would have to spend 30 to 45 hours a week on a module to be able to pass the module. First year module have a low passing requirement pf 30-40% depending on the module in the whole academic year to be able to progress to the next stage of the degree.

  Since this report is on the **Computer Science (BSc)** degree here is a overview of the what the course is :- Computer science is a diverse subject with lots of application that is built on a foundation of topics such as computational thinking, information processing, computer design computational programming and generating algorithms to solving problems given that the problem can be computational solved in a finite number of steps. An algorithm is a set of tasks/steps that the computer has to carry out in order to for it to perform and complete a task; to do this the algorithm must be presented in a form that the machine is compatible with. People who write and fix algorithms are know as computer scientist. 

Computer scientist depend on their ability to identify problems and how to solve them in a computational way (which is a way a computer might be able to solve the problem by following certain set of steps). People who design and write computer programs are know as software engineers or programmers who use different IDE’s and programming languages to solve problems in a set number of steps called an algorithm.

As discussed, Computer Science is a complicated course which needs a high level of understanding and flexibility in order to be successful in the field. Companies accept employees as intern computer scientist without a computing degree, however these people get paid lesser on average when starting then a computer science graduate would when starting out; as having a academic degree is generally regarded as being idle and most sort after way to get a job as shown by the easier entry to jobs and the increase in people attending universities _“in the early Sixties only 6 per cent of under-21s went to university. Today, the figure stands closer to 43 per cent.”_ **[2]** what more is that the higher and in-depth expertise acquired from degree pose a greater value as a professional in general.

Choosing to pursue a degree has other benefits other than just employment as working in an environment with other like minded individual on the same tasks builds a persons social and team working skills while also allowing people to make connections and contact with people who could be useful and successful in the future and could provide opportunities that people who didn’t go to university would have access to. Research has also proved that _“Starting salaries vary a lot for this type of work as the roles are so varied, but typically they'll be around **£25,000**. It's worth mentioning that despite this average, graduates have reported receiving anything from **£17,000 to £70,000** in IT roles.”_ **[3]**

However even something as good as a degree has many cons and limitations which have to be assessed and weighted while deciding while planning on weather to chose to pursue a degree or not. A major that arises when looking to go to university is the time. Universities have a minimum of 3 of study before acquiring a degree however this is only the minimum and the degree like a medical doctor could take from anywhere between 9 to 15 years depending on the degree, a lot of people can be put off by the idea of spending large amounts of time to pursue a degree and this is worse if people take into account that there is still a chance that they might be unemployed for large amounts of time after getting a degree if there are lots of people already in that field. Another factor id the cost of the course itself a degree of 3 years could cost around 30k and that’s without rent, living expenses, additional book and resource fees and finally travel cost, most people would not be able to pay these large sums off money on their own and so there are loans for such people however being indebt is never a good idea specially if the student already comes from a unstable financial family to begin with. 

To conclude even with the various advantages and disadvantages of getting a degree the choice comes down to the individual in question and how their situation and condition allows to to pick whether or not they want to do a degree. This decision should be concluded after a thorough examinations, planning and research. 

Finally, I will detail the next bit of the report on my own personal experience of the computer science degree and provide the results of my research in to the computer science course and specification in order to deem if the course is worth the cons. Breaking down the computer science course becomes useful for understanding and planning ahead. As viewing the milestones and breaking down times and allocations to specific models for the assessments and exams helps ease the process of moving to university degrees from A-Levels which is a drastic change in the quantity of the content while also having more specific and higher standards of works. Using the report as a plan is highly valuable and insightful in order to ease the transition as much as possible.

The following structures have been used to decompose and visualize the course below.
*	A **Work Breakdown Structure (WBS)** is used to breakdown the **Computer Science (BSc)** degree into the different years and the specific modules that are to be undertaken in those specific years this include the optional and compulsory modules. The diagram represents the whole course as a abstract view with fewer detail then there originally is as the **WBS** aims on not confusing the reader by adding too much information that they will not understand. As a student using this allows me to see how the deferent year are connected and how modules carry over or prepare me as the student to work with the module in the coming years which let me focus on the desired outcomes for the specific years rather then the actions needed to complete them.
*	**Gantt Charts** are used to represent each of the academic years and terms as 4 different Gantt diagrams. _“A Gantt chart is a type of bar chart that illustrates a project schedule, named after its inventor, Henry Gantt, who designed such a chart around the years 1910–1915. Modern Gantt charts also show the dependency relationships between activities and current schedule status.”_ **[4]** . the first Gantt chart represent the whole course as term dates and when each term in each of the 3 years starts and ends. The three following Gantt charts represent the milestones and dependencies of the each of the individual 3 years as dates for assessments, coursework and online tests and weights of the autumn terms.

These structure provide a valuable and insightful overview and feedback about the course and creating a learning plan to insure the best possible experience of the course by keeping up to the plan.



## Work Breakdown Structure (WBS) diagram

![](wbs1.png)

## Gantt Chart diagram - overview

<div align="center">

![](ganttoverview.png)

</div>

### Gantt Chart diagram - Year 1

<div align="center">  

![](gantt1.png)

</div>

### Gantt Chart diagram - Year 2

<div align="center">  

![](gantt2.png)

</div>

### Gantt Chart diagram - Year 3

<div align="center"> 

![](gantt3.png)


</div>

## reflection

In this part of the report I will reflect on I have done and achieved and where I went wrong with how I overcame this to end up with a suitable learning plan for the computer science course. To start of I will detail the first problem I came across was using a ‘class diagram’ which at first seemed a great idea to implement the **Work Breakdown Structure (WBS)** after intensive research into suitable methods on different ways to implement a WBS by google and a few recommendations from my fellow peers. I had prier knowledge on a class diagram from A-level computer science as we used this to abstract a programming into a parent-class/superclass and its sub-classes or otherwise known as children class. The definition of a class diagram is _“In software engineering, a class diagram in the Unified Modelling Language is a type of static structure diagram that describes the structure of a system by showing the system's classes, their attributes, operations, and the relationships among objects.”_ **[5]**. Using this structure I could make the course (**Computer Science (BSc)**) as the superclass and break down the degree into subclasses like each of the years of the course, I could then break the individual years into further subclasses as the different modules and keep breaking down the subclass until each subclass is an individual independent part of the course in the simplest way. However, there was a problem that arise that was unexpected, on preview of the work after committing changes I noticed that the class diagram was not scaled properly. Given this recent predicament I decided to research up solutions for the diagram however I wasn’t able to find a suitable and easy to implement solution to the problem, at this point this approach as proving to very time intensive to I decided to abandon it and I then stumbled upon the **SmartArt** option on **Microsoft Word**  which allowed me to achieve the same results in a visual format that the class diagram offered in a simpler and quicker method by using the hierarchy diagram, which is why I then began implementing my **Work Breakdown Structure (WBS)** on the hierarchy diagram which only took a few minutes to implement and complete the WBS and the result turned out better then expected given the short time frame it took to making it. The result was a very simple and user-friendly diagram that was not hard on the eye in terms of content but still had the important relevant data that was core to understanding the structure of the course and building a suitable learning plan.

The next step I implemented was the creation of the **Gantt Chart**, for this I decided to break the course into 4 different **Gantt Chart** to simply the course and spread out the data across their respective years to make them easier to read. The first chart was an overview of all the terms in the 3 years and how long each term was and when it starts and ends, the next 3 are similar in nature as each of them contain the compulsory modules of their respective years and deadlines of milestones like assessments and test or coursework of the autumn term. Using the chart was for me personally the most helpful and productive part of the report as the charts made clear not just the content covered and assessments but also broke down how and when these milestones and assessments are carried out allowing, we to better plan for each one well ahead of time by managing complex information better.

Another reason using **Gantt Chart** was beneficial was because it allows better resource planning as this can be managed thanks to how the timelines work in Gantt charts. This gives student like me a chance to plan better as they can manage their time effectively and understand the milestones and deadlines efficiently. Learning from my previous mistake with the class diagram this time I chose to research not only the best solution but also possible pros and cons of using a specific method to implement the **Gantt Chart** and weigh these options to choose a method that was not only easy to read and understand but also was time effective as I spent too much time obsessing over how I wanted the report to look and be structured rather then researching and implementations the individuals parts over the reading week instead of only planning in that week and trying to fit all the work in a few days before the deadline which left me short on time that I could afford to lose implementing fancy complicated charts which require lots of research to implement. So, upon research on the topic I discovered a YouTube video that allows you to make grant charts on excel, hence because of my previous experience in excel and how using excel allowed me to break the individual milestones which in this case are the assessments, coursework and online test separately. I had no problems implementing the **Gantt Chart** and completed them without any drawbacks

That being said life if never that easy and simply and eventually I noticed another problem with the WBS and Gantt chart which was that they were diagrams and **CSGitlab** only allows for images and pdfs to be uploaded and saved to the repository of the file. However, these problems which could have been fatal at first forcing me to rewrite all my work again had an easy and quick fix which upon learning felt like a mountain of pressure had been lifted from my shoulders. Without further suspense I will reveal my revelation which was as basic as storing the diagrams on word as they were then copying the diagrams one by one and pasting them as images back into word and saving each of them as images onto the pictures. Next steps was uploading the pictures to my repository and using the _‘![](imagine name goes here)’_ command in markdown to make the picture appear in that lactation.

To conclude working on this report has helped better my understanding and structure of the degree and how I could create a learning plan using the information provided and research done into the degree. Not only has this report helped with my understanding of the course and helped me towards creating a suitable learning plan is has also proven helpful to me as a professional software engineering and other relate field by helping improve my researching skills and how to structure and work on projects for future references. I have learned that I shouldn’t just pick the best possible result google offers but use feedback from my peers or people online as to the possible drawback of using a chosen method or structure which might not be apparent at first which could cause a cascading effect of rendering my report useless and forcing me to start again. Another thing I learnt from this report is learning to manage my time better and not leaving the reports and research just a time span of a few days under which a report can be created but under very stressful situations which could cause errors and a non-professional report.



## References

**[1]** [allabout, [Accessed 11th november 2020]](https://www.allaboutschoolleavers.co.uk/school-leaver-options/degree/what-is-a-degree)

**[2]** [independant, [Accessed 11th november 2020]](https://www.independent.co.uk/student/career-planning/getting-job/does-a-degree-guarantee-you-a-good-job-795996.html)

**[3]** [save the student, [Accessed 12th november 2020]](https://www.savethestudent.org/student-jobs/whats-the-expected-salary-for-your-degree.html)

**[4]** [Wikipedia, [Accessed 12th november 2020]](https://en.wikipedia.org/wiki/Gantt_chart)

**[5]** [Wikipedia, [Accessed 12th november 2020]](https://en.wikipedia.org/wiki/Class_diagram)
